<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>The XMLHttpRequest Object</title>
</head>

<body>
    <h1>The XMLHttpRequest Object</h1>
    <textarea name="demo" id="demo" cols="60" rows="10">
        Let AJAX change this text.
    </textarea>
    <button type="button" onclick="loadDoc()">Change Content</button>

    <script>
        function loadDoc() {
            alert("Hello");
            var xhttp;
            if (window.XMLHttpRequest) {
                //code for modern browsers
                xhttp = new XMLHttpRequest();
            } else {
                //code for IE6, IE5
                xhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("demo").innerHTML = this.responseText;
                }
            };
            xhttp.open("GET", "demo.txt", true);
            xhttp.send();
        }
    </script>
</body>

</html>